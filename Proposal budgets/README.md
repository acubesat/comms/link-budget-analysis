## Proposal Link Budget Analysis

This repository contains the link budget analysis that was conducted for the AcubeSAT proposal. The excel sheets include:

* Link Models for the AcubeSAT UHF and S band antennas
* Worst case analysis for the S band antenna

